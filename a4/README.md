# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Assignment 4 Requirements:

*Two parts*:

1. Basic server-side validation and assembly + compilation of servlet files
2. Chapter questions (Chs. 11, 12)
	
	
#### README.md file should include the following items:

* Screenshot of http://localhost:9999/lis4368/customerform.jsp main page
* Screenshots of http://localhost:9999/lis4368/customerform.jsp failed and passed validation
* Screenshots of skill sets 10, 11, and 12

#### Assignment Screenshots:

*Screenshots of http://localhost:9999/lis4368/customerform.jsp's Main Page, Failed Validation, and Passed Validation*:

| Main Page | Failed Validation | Passed Validation |
| :---: | :---: | :---: |
| ![Screenshot of the main page](img/main.PNG) | ![Screenshot of failed validation](img/failedvalidation.PNG) | ![Screenshot of passed validation](img/passedvalidation.PNG) |

*Screenshots of Skill Sets*:

| Skill Set 10 | Skill Set 11 | Skill Set 12 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 10 running](img/CountCharacters.PNG) | ![Screenshot of Skill Set 11 running](img/FileWriteReadCountWords.PNG) | ![Screenshot of Skill Set 12 running](img/Ascii.PNG) |