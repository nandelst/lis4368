# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Project 2 Requirements:

*Two parts*:

1. Completed CRUD functionality for database
2. Chapter questions (Chs. 16, 17)

	
#### README.md file should include the following items:

* Screenshots of http://localhost:9999/lis4368/customerform.jsp's valid entry, passed validation, and entry on the customer table
* Screenshots of http://localhost:9999/lis4368/customerAdmin's modify form entry, and modified entry on the customer table
* Screenshots of http://localhost:9999/lis4368/customerAdmin?action=display_customers' delete prompt and the entry gone from the customer table
* Screenshots of associated database entries

#### Assignment Screenshots:

*Screenshots of http://localhost:9999/lis4368/customerform.jsp's valid entry, passed validation, and entry on the customer table*:

| Valid entry | Passed validation | Customer table
| :---: | :---: | :---: |
| ![Screenshot of valid entry](img/validentry.PNG) | ![Screenshot of passed validation](img/passedvalidation.PNG) | ![Screenshot of customer table](img/displaydata.PNG) |

*Screenshots of http://localhost:9999/lis4368/customerAdmin's modify form entry, and modified entry on the customer table*:

| Modify form entry | Customer table |
| :---: | :---: |
| ![Screenshot of modify form entry](img/modifyform.PNG) | ![Screenshot of customer table](img/modifieddata.PNG) |

*Screenshots of http://localhost:9999/lis4368/customerAdmin?action=display_customers's delete prompt and the entry gone from the customer table*:

| Delete prompt | Customer table |
| :---: | :---: |
| ![Screenshot of delete prompt](img/deletewarning.PNG) | ![Screenshot of customer table](img/deleted.PNG) |

*Screenshots of associated database entries*:

| Part 1 | Part 2 | 
| :---: | :---: | 
| ![Screenshot of valid entry](img/database1.PNG) | ![Screenshot of valid entry](img/database2.PNG) |



