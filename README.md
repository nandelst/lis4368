> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install JDK
	- Install Tomcat
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Java servlet
	- Create database servlet
	- Provide screenshots of servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create pet store ERD
	- Provide links to a3.mwb and a3.sql

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Finish customerform.jsp
	- Finish customer.jsp
	- Finish and compile CustomerServlet.java
	- Finish and compile Customer.java

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Create and compile ConnectionPool
	- Create and compile CustomerDB
	- Create and compile DBUtil
	- Allow for data to be entered into the MySQL database

6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Add jQuery validation to p1/index.jsp
	- Add regular expressions to p1/index.jsp

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Add CRUD functionality to database
	- Update and recompile CustomerDB.java and CustomerServlet.java
	- Update modify.jsp and customers.jsp