# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Assignment 3 Requirements:

*Three Parts:*

1. Completed ERD with MySQL Workbench
2. Forward engineered database with 10 records in each table
3. Chapter questions (Chs. 7, 8)
	
#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of http://localhost:9999/lis4368/a3/index.jsp
* Links to the following files:
	* a3.mwb
	* a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/erd.PNG)

*Screenshot of http://localhost:9999/lis4368/a3/index.jsp*:

![Screenshot of assignment 3 index.jsp](img/index.PNG)

#### Assignment Links:

*Link to a3.mwb*:
[a3.mwb](docs/a3.mwb "Link to a3.mwb")

*Link to a3.sql*:
[a3.sql](docs/a3.sql "Link to a3.sql")