# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Project 1 Requirements:

*Two Parts:*

1. Basic client-side validation using regular expressions and jQuery validation
2. Chapter questions (Chs. 9, 10)
	
#### README.md file should include the following items:

* Screenshot of http://localhost:9999/lis4368/ portal (main/splash page)
* Screenshots of http://localhost:9999/lis4368/p1/index.jsp failed and passed validation

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/lis4368/ portal (main/splash page)*:

![Screenshot of LIS4368 portal](img/portal.PNG)

*Screenshots of http://localhost:9999/lis4368/p1/index.jsp failed and passed validation*:

| Failed validation | Passed Validation |
| :---: | :---: |
| ![Screenshot of failed validation](img/failedvalidation.PNG) | ![Screenshot of passed validation](img/passedvalidation.PNG) |