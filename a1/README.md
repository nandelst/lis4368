> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running Java Hello (#1 above)
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial)
* Screenshot of local lis4368 web application and link to local lis4368 web application
* git commands w/ short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial (bitbucketstationlocations).

#### Git commands w/short descriptions:

1. git init - Initializes a new Git repository.
2. git status - Lets you see the status of your repository such as new changes or tracked files.
3. git add - Stages a specific file or your directory for updates.
4. git commit - Saves your updates to the local repository.
5. git push - Uploads your local repository to a remote one.
6. git pull - Downloads a remote repository and updates your local repository.
7. git tag - Adds a reference number to a specific file to mark a version of it.

#### Assignment Screenshots:

*Screenshot of running Java Hello*:

![Screenshot of Java Hello](img/jdk_install.PNG)

*Screenshot of running http://localhost:9999*:

![Screenshot of running Tomcat localhost](img/tomcat.PNG)

*Screenshot of local LIS4368 web appplication*:

![Screenshot of local LIS4368 web application](img/a1.PNG)

#### Tutorial Links:

*Local LIS4368 Web Application*:
[Link to local LIS4368 Web Application](http://localhost:9999/lis4368/ "My LIS4368 localhost Web Application")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nandelst/bitbucketstationlocations/ "Bitbucket Station Locations")
