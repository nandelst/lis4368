# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Assignment 2 Requirements:

*Two Parts:*

1. More Java/JSP/Servlet Development
2. MySQL Implementation

#### README.md file should include the following items:

* Screenshots of all http://localhost:9999 assessments (hello, HelloHome, sayhello, sayhi, and querybook)
* Screenshot of query results from http://localhost:9999/hello/querybook.html
* Screenshot of http://localhost:9999/lis4368/a2/index.jsp

#### Assignment Screenshots:

*Screenshots of running http://localhost:9999/hello and http://localhost:9999/HelloHome.html (directory picture included)*:

| Hello directory | HelloHome servlet | Renamed to index.html |
| :---: | :---: | :---: |
| ![Screenshot of the hello directory](img/hello.PNG) | ![Screenshot of running the HelloHome servlet](img/HelloHome.PNG) | ![Screenshot of renamiing HellomeHome.html to index.html](img/hello2.PNG) |

*Screenshots of running http://localhost:9999/hello/sayhello and http://localhost:9999/hello/sayhi*:

| Sayhello servlet | Sayhi servlet |
| :---: | :---: |
| ![Screenshot of http://localhost:9999/hello/sayhello](img/sayhello.PNG) | ![Screenshot of http://localhost:9999/hello/sayhi](img/sayhi.PNG) |

*Screenshots of running http://localhost:9999/hello/querybook.html*:

| Query selection | Query results |
| :---: | :---: |
| ![Screenshot of the query selection page](img/querybook.PNG) | ![Screenshot of the query results](img/querybook2.PNG) |

*Screenshot of http://localhost:9999/lis4368/a2/index.jsp*:

![Screenshot of assignment 2 index.jsp](img/index.PNG)
