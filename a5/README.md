# LIS4368 - Advanced Web Applications Development

## Douglas Nandelstadt

### Assignment 5 Requirements:

*Two parts*:

1. Basic server-side validation and database entry
2. Chapter questions (Chs. 13, 14, 15)

	
#### README.md file should include the following items:

* Screenshots of http://localhost:9999/lis4368/customerform.jsp's valid entry and passed validation
* Screenshot of Associated Database Entry
* Screenshots of skill sets 13, 14, and 15

#### Assignment Screenshots:

*Screenshots of http://localhost:9999/lis4368/customerform.jsp's valid entry and passed validation*:

| Valid Entry | Passed Validation | 
| :---: | :---: | 
| ![Screenshot of valid entry](img/validentry.PNG) | ![Screenshot of passed validation](img/passedvalidation.PNG) |

*Screenshot of associated database entry*:

![Screenshot of Associated Database Entry](img/databaseentry.PNG)

*Screenshots of Skill Sets*:

| Skill Set 13 | Skill Set 14 | Skill Set 15 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 13 running](img/NumberSwap.PNG) | ![Screenshot of Skill Set 14 running](img/LargestOfThreeNumbers.PNG) | ![Screenshot of Skill Set 15 running](img/SimpleCalculatorVoidMethods.PNG) |
